from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.datetime_safe import datetime


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64, unique=True)
    category = models.CharField(max_length=64, null=True)
    description = models.TextField(default='')
    price = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)

    class Meta:
        ordering = ('name',)
        verbose_name_plural = 'Products'

    def __str__(self):
        return f"{self.name}"

class Cart(models.Model):
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.SET_NULL)
    count = models.PositiveIntegerField(default=0)
    total = models.DecimalField(default=0.00, max_digits=10, decimal_places=2)
    updated = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('updated',)
        verbose_name_plural = 'Carts'

    def __str__(self):
        return f"{self.user}"



class Entry(models.Model):
    cart = models.ForeignKey(Cart, null=True, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, null=True, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()

    class Meta:
        ordering = ('product',)
        verbose_name_plural = 'Entries'

    def __str__(self):
        return f"{self.product.name} ({self.cart.user})"

@receiver(post_save, sender=Entry)
def update_cart(sender, instance, **kwargs):
    line_cost = instance.quantity * instance.product.price
    instance.cart.total += line_cost
    instance.cart.count += instance.quantity
    instance.cart.updated = datetime.now()