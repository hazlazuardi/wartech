from django.http import HttpResponse
from django.shortcuts import render
from .models import Cart, Entry, Product
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
import json

# So, the flow is like this:
# 1. Every users that are logged in will have their own unique, just one, cart.
# 2. The way to fill the cart is by having Entries. So, Cart will only
# count how many items are there and the total price.
# 3. Entries, however, just the lists of what product is added to the cart,
# for every user. So, cart here works like categories.
# 4. Everytime user add a product wto the cart, new entry is created. Then, we 
# pass the entry to the cart so the count inside cart is incremented.

# Implement Search Products
def dashboard(request):
    allProduct = Product.objects.all()
    context={
        'products':allProduct,
        # 'form': SearchForm()
    }
    # form = SearchForm()
    if request.method == 'POST':
        # Get the POST from AJAX, as a search input
        keyword = request.POST['nama_barang']
        # Get Product contains the search keyword
        searchProduct_name = Product.filter(name__icontains=keyword)
        searchProduct_category = allProduct.filter(category__icontains=keyword)
        # Replace allProduct with the Keyword Product
        context['products'] = searchProduct_name | searchProduct_category
        # Return JSON data for Javascript
        return HttpResponse(json.dumps(allProduct))
    return render(request,"dashboard/index.html",context)

def productDetail(request, index):
    product = Product.objects.get(pk=index)
    context = {
        'product':product
    }
    return HttpResponse(f"Nama produk: {product}")

@login_required(redirect_field_name='LOGIN_REDIRECT_URL')
def addCart(request):
    # Based on the user who is making the request, grab the cart object
    # .get_or_create will create a tuple with (<object>, created=boolean)
    # Hence, we should break it down
    my_cart, created = Cart.objects.get_or_create(user=request.user)
    # Get entries in the cart
    my_carts_current_entries = Entry.objects.filter(cart=my_cart)
    # Get a list of your products
    products = Product.objects.all()
    if request.POST:
        # Get the product's ID from the POST request.
        product_id = request.POST.get('product_id')
        # Get the object using our unique primary key
        product_obj = Product.objects.get(id=product_id)
        # Get the quantity of the product desired.
        product_quantity = request.POST.get('product_quantity')
        # Create the new Entry...this will update the cart on creation
        Entry.objects.create(cart=my_cart, product=product_obj, quantity=product_quantity)
        return HttpResponse('dah di add')

    return HttpResponse()


# Functions: 
# A. Add to Cart 
# B. Search Item 
# C. Detail Product
# (Optional)
# D. Show Rank
# E. Like the Item