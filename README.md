# Wartech
This Gitlab repository is the result of the work from **Wishnu Hazmi**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/hazlazuardi/wartech/badges/master/pipeline.svg)](https://gitlab.com/hazlazuardi/wartech/commits/master)
[![coverage report](https://gitlab.com/hazlazuardi/wartech/badges/master/coverage.svg)](https://gitlab.com/hazlazuardi/wartech/commits/master)

## URL
This story can be accessed from [https://hazlazuardi.herokuapp.com](https://wartech.herokuapp.com)

## Author
**Wishnu Hazmi** - [hazlazuardi](https://gitlab.com/hazlazuardi)